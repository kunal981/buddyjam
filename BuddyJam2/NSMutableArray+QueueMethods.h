//
//  NSMutableArray+QueueMethods.h
//  TDAudioPlayer
//
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (QueueMethods)

- (void)pushObject:(id)object;
- (id)popObject;
- (id)topObject;

@end
