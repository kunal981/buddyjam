//
//  ConnectedViewController.m
//  BuddyJam2
//
//  Created by brst on 1/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ConnectedViewController.h"
#import "ViewController.h"
#import "PlayerVC.h"

@interface ConnectedViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    NSArray *deviceList;
    UIAlertView *alert;

}

@end

@implementation ConnectedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    nextButton.layer.cornerRadius=4.0;
    tableView.layer.cornerRadius=4.0;
    deviceList=[self.session connectedPeers] ;
    tableView.delegate=self;
    tableView.dataSource=self;
    NSLog(@"coonected peers=%@",deviceList);
    
    alert=[[UIAlertView alloc]initWithTitle:nil message:@"Please Connect to atleast one device" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate=self;
    
    
   
}
-(void)viewWillAppear:(BOOL)animated{
  /*  if([self.session connectedPeers].count<1)
    {
        tableView.hidden=YES;
        [alert show];
    }
    else{
        tableView.hidden=NO;
    }*/
    

    [self frameSetting];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(deviceList.count>0)
        return  deviceList.count;
    
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=nil;
    UITableViewCell *cell=[tableView1 dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        
    }
    cell.textLabel.text= [[deviceList objectAtIndex:indexPath.row]displayName];
    
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0)
    {
        // if([self.session connectedPeers].count<=1)
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


- (IBAction)homeBtnPressed:(id)sender {
    self.session=nil;
   [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)nextBtnPressed:(id)sender {
    if([self.session connectedPeers].count>0)
    {
        PlayerVC *playerObj=[[PlayerVC alloc]init];
        playerObj.session=self.session;
        [self.navigationController pushViewController:playerObj animated:YES];
    }
    else
    {
        [alert show];
    }
    
}
-(void)frameSetting
{
    [backgroundImage setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    int height=SCREEN_HEIGHT;
    
    switch(height)
    {
            
        case 480:
            [buddyImage setFrame:CGRectMake(40, 20, self.view.frame.size.width-80,80)];
            [connectedLbl setFrame:CGRectMake(0, 110, self.view.frame.size.width,connectedLbl.frame.size.height)];
            
            [tableView setFrame:CGRectMake(10, 140, self.view.frame.size.width-20,150)];
            
            [nextButton setFrame:CGRectMake(100,300, nextButton.frame.size.width,nextButton.frame.size.height)];
            [homeBtn setFrame:CGRectMake(130, self.view.frame.size.height-homeBtn.frame.size.height-60, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            break;
            
        case 736:
            [buddyImage setFrame:CGRectMake(40, 30, self.view.frame.size.width-80,130)];
            [connectedLbl setFrame:CGRectMake(0, 180, self.view.frame.size.width,connectedLbl.frame.size.height)];
            
            [tableView setFrame:CGRectMake(10, 230, self.view.frame.size.width-20,230)];
            
            [nextButton setFrame:CGRectMake(140, 480, nextButton.frame.size.width,nextButton.frame.size.height)];
            [homeBtn setFrame:CGRectMake(175, self.view.frame.size.height-homeBtn.frame.size.height-90, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            break;
            
         case 667:
           
            [buddyImage setFrame:CGRectMake(40, 30, self.view.frame.size.width-80,130)];
            [connectedLbl setFrame:CGRectMake(0, 180, self.view.frame.size.width,connectedLbl.frame.size.height)];
            [tableView setFrame:CGRectMake(10, 230, self.view.frame.size.width-20,230)];
            [nextButton setFrame:CGRectMake(130, 470, nextButton.frame.size.width,nextButton.frame.size.height)];
            [homeBtn setFrame:CGRectMake(160, self.view.frame.size.height-homeBtn.frame.size.height-80, homeBtn.frame.size.width,homeBtn.frame.size.height)];
          
    }
    
}

@end
