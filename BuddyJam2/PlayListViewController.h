//
//  PlayListViewController.h
//  BuddyJam
//
//  Created by brst on 1/12/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@import MediaPlayer;
@protocol PlayListViewControllerDelegate; // forward declaration

@interface PlayListViewController : UIViewController
{
    id <PlayListViewControllerDelegate>	delegate;
    MPMediaItemCollection *currentQueue ;
    IBOutlet UILabel *musicLbl;
    IBOutlet UIButton *doneBtn;
}

@property (strong, nonatomic) IBOutlet UITableView *playList;
@property (strong, nonatomic) MPMediaItemCollection *currentQueue;
- (IBAction)DonePressed:(id)sender;

@end
