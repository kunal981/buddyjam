//
//  CreateViewController.m
//  BuddyJam2
//
//  Created by brst on 1/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "CreateViewController.h"
#import "ViewController.h"
#import "ConnectedViewController.h"
#import "PeerSession.h"

@interface CreateViewController ()<UITextFieldDelegate>
{
    ConnectedViewController *connectedObj;
}
@property (strong, nonatomic) PeerSession *session;
@end

@implementation CreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    startBtn.layer.cornerRadius=4.0;
    groupName.delegate=self;
    groupPassword.delegate=self;
    groupPassword.tag=2;
    groupName.returnKeyType=UIReturnKeyNext;
    groupPassword.secureTextEntry=YES;
   
    NSLog(@"height=%f width=%f", self.view.frame.size.height,self.view.frame.size.width);
    [switchBtn addTarget:self action:@selector(swithBtnChange) forControlEvents:UIControlEventValueChanged];
   
}
-(void)swithBtnChange
{
    if([switchBtn isOn])
    {
         groupPassword.secureTextEntry=YES;
    }else{
        groupPassword.secureTextEntry=NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
    

-(void)viewWillAppear:(BOOL)animated{
    [self frameSetting];
    groupName.text=@"";
    groupPassword.text=@"";
}


- (IBAction)homeBtnPressed:(id)sender {
    self.session=nil;
    [self.navigationController popToRootViewControllerAnimated:YES];
    NSLog(@"123");
    

}


- (IBAction)startBtnPressed:(id)sender {
    
    NSString *name=[groupName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *pswd=[groupPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
   if(name.length>0 && pswd.length>0)
    {
        [[NSUserDefaults standardUserDefaults]setObject:groupName.text forKey:@"Jam"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *jam=[NSString stringWithFormat:@"%@ Jam",groupName.text.capitalizedString];
        
        self.session = [[PeerSession alloc] initWithPeerDisplayName:jam];
      //  self.session = [[PeerSession alloc] initWithPeerDisplayName:[UIDevice currentDevice].name];
        [self presentViewController:[self.session browserViewControllerForSeriviceType:@"dance-party"] animated:YES completion:nil];
        
        if(!connectedObj)
            connectedObj=[[ConnectedViewController alloc]init];
        
        connectedObj.session=self.session;
        NSLog(@"%lu",(unsigned long)[self.session connectedPeers].count);
        [self.navigationController pushViewController:connectedObj animated:YES];
         NSLog(@"123");
    }
    else
    {
        UIAlertView *alert;
        NSString *msg;
        if(name.length<1 && pswd.length<1)
            msg=@"Please fill Jam Name and Password ";
        else if(name.length<1)
             msg=@"Please fill JAM Name";
        else if(pswd.length<1)
            msg=@"Please fill Password Field";
        
        
        if(!alert)
           alert=[[UIAlertView alloc]initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }

    
}



-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField.tag==2)
      [textField resignFirstResponder];
    else
        [groupPassword becomeFirstResponder];
    return NO;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    int height=SCREEN_HEIGHT;
    if(height==480)
    {
        if(textField.tag==2)
        {
            CGRect frame=self.view.frame;
            frame.origin.y -= 50;
            [self.view setFrame:frame];
            
        }
 
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    int height=SCREEN_HEIGHT;
    if(height==480)
    {
        if(textField.tag==2)
        {
            CGRect frame=self.view.frame;
            frame.origin.y += 50;
            [self.view setFrame:frame];
            
        }
        
    }

}



-(void)frameSetting
{
    [backgroundImage setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    int height=SCREEN_HEIGHT;
    NSLog(@"height=%f width=%f", SCREEN_HEIGHT,SCREEN_WIDTH);
    switch(height)
    {
            
        case 480:
            [buddyImage setFrame:CGRectMake(80,40, buddyImage.frame.size.width,100)];
            [createLbl setFrame:CGRectMake(self.view.frame.origin.x,150 , self.view.frame.size.width,createLbl.frame.size.height)];
            [switchBtn setFrame:CGRectMake(80, 215, switchBtn.frame.size.width,switchBtn.frame.size.height)];
            [passwordLbl setFrame:CGRectMake(140, 220, passwordLbl.frame.size.width,passwordLbl.frame.size.height)];
            [groupName setFrame:CGRectMake(10,180, self.view.frame.size.width-20,groupName.frame.size.height)];
            [groupPassword setFrame:CGRectMake(10, 250, self.view.frame.size.width-20,groupPassword.frame.size.height)];
            [startBtn setFrame:CGRectMake(100, 300, startBtn.frame.size.width,startBtn.frame.size.height)];
            [homeBtn setFrame:CGRectMake(130, self.view.frame.size.height-homeBtn.frame.size.height-60, homeBtn.frame.size.width,homeBtn.frame.size.height)];

            break;
            
        case 736:
            [buddyImage setFrame:CGRectMake(130, 80, buddyImage.frame.size.width,buddyImage.frame.size.height)];
            [createLbl setFrame:CGRectMake(self.view.frame.origin.x, 230, self.view.frame.size.width,createLbl.frame.size.height)];
            [switchBtn setFrame:CGRectMake(140, 315, switchBtn.frame.size.width,switchBtn.frame.size.height)];
            [passwordLbl setFrame:CGRectMake(200, 320, passwordLbl.frame.size.width,passwordLbl.frame.size.height)];
            [groupName setFrame:CGRectMake(self.view.frame.origin.x+10, 270, self.view.frame.size.width-20,groupName.frame.size.height)];
            [groupPassword setFrame:CGRectMake(self.view.frame.origin.x+10, 360, self.view.frame.size.width-20,groupPassword.frame.size.height)];
            [startBtn setFrame:CGRectMake(150, 430, startBtn.frame.size.width,startBtn.frame.size.height)];
            [homeBtn setFrame:CGRectMake(175, self.view.frame.size.height-homeBtn.frame.size.height-90, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            break;
            
        case 667:
            [buddyImage setFrame:CGRectMake(110, 80, buddyImage.frame.size.width,buddyImage.frame.size.height)];
            [createLbl setFrame:CGRectMake(self.view.frame.origin.x, 230, self.view.frame.size.width,createLbl.frame.size.height)];
            [switchBtn setFrame:CGRectMake(120, 315, switchBtn.frame.size.width,switchBtn.frame.size.height)];
            [passwordLbl setFrame:CGRectMake(180, 320, passwordLbl.frame.size.width,passwordLbl.frame.size.height)];
            [groupName setFrame:CGRectMake(self.view.frame.origin.x+10, 270, self.view.frame.size.width-20,groupName.frame.size.height)];
            [groupPassword setFrame:CGRectMake(self.view.frame.origin.x+10, 360, self.view.frame.size.width-20,groupPassword.frame.size.height)];
            [startBtn setFrame:CGRectMake(130, 415, startBtn.frame.size.width,startBtn.frame.size.height)];
            [homeBtn setFrame:CGRectMake(160, self.view.frame.size.height-homeBtn.frame.size.height-80, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            break;
            
        case 1024:
            [buddyImage setFrame:CGRectMake(self.view.frame.size.width/2-80, 80, 160,200)];
            [createLbl setFrame:CGRectMake(self.view.frame.origin.x, 320, self.view.frame.size.width,createLbl.frame.size.height)];
            [groupName setFrame:CGRectMake(self.view.frame.origin.x+10, 400, self.view.frame.size.width-20,groupName.frame.size.height)];
            [groupPassword setFrame:CGRectMake(self.view.frame.origin.x+10, 490, self.view.frame.size.width-20,groupPassword.frame.size.height)];

            [switchBtn setFrame:CGRectMake(self.view.frame.size.width/2-80, 450, switchBtn.frame.size.width,switchBtn.frame.size.height)];
            [passwordLbl setFrame:CGRectMake(self.view.frame.size.width/2-20, 455, passwordLbl.frame.size.width,passwordLbl.frame.size.height)];
                        [startBtn setFrame:CGRectMake(130, 415, startBtn.frame.size.width,startBtn.frame.size.height)];
            [homeBtn setFrame:CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height-homeBtn.frame.size.height-150, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            [startBtn setFrame:CGRectMake(self.view.frame.size.width/2-startBtn.frame.size.width/2, 550, startBtn.frame.size.width,startBtn.frame.size.height)];
            break;
            
            
        default:
        {
            
        }
    }
    
}

@end
