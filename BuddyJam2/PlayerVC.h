//
//  PlayerVC.h
//  BuddyJam2
//
//  Created by brst on 1/16/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PeerSession.h"
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
@import MediaPlayer;


@interface PlayerVC : UIViewController<MPMediaPickerControllerDelegate>
{
    IBOutlet UILabel *songName;
    IBOutlet UIButton *homeBtn;
    IBOutlet UIImageView *backgroundImage;
    IBOutlet UIButton *playBtn;
    IBOutlet UIButton *playList;
    IBOutlet UILabel *authorName;
    IBOutlet UIImageView *songImage;
    MPMediaItemCollection *userSelectedPlayList;
    MPMusicPlayerController		*musicPlayer;
    IBOutlet UIButton *forward;
    IBOutlet UIButton *backwardBtn;
    UIView *volumeHolder;
    IBOutlet UIImageView *minVolumeImage;
    IBOutlet UIImageView *maxValumeImage;
    
}
@property (strong, nonatomic) PeerSession *session;
@property (retain, nonatomic) MPMediaItemCollection *userSelectedPlayList;
@property (nonatomic, retain)	MPMusicPlayerController	*musicPlayer;
- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)playListBtn:(id)sender;
- (IBAction)playBtn:(id)sender;
- (IBAction)backward:(id)sender;
- (IBAction)forward:(id)sender;


@end
