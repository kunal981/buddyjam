//
//  PlayerVC.m
//  BuddyJam2
//
//  Created by brst on 1/16/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "PlayerVC.h"
#import "TDAudioStreamer.h"
#import "PlayListViewController.h"
#import <MediaPlayer/MediaPlayer.h>
//#import <AudioToolbox/AudioToolbox.h>

@import AVFoundation;
static int count=0;
BOOL isPlaying=NO;
BOOL isBackwardPressed=0,isForwardPressed=0;
NSUInteger currentPlayingSongIndex=0;
@interface PlayerVC ()
{
    NSUserDefaults *userDefault;
    NSString *selectedSong;
    UIAlertView *alert;
}
@property (strong, nonatomic) MPMediaItem *song;
@property (strong, nonatomic) TDAudioOutputStreamer *outputStreamer;
@property (strong, nonatomic) AVPlayer *player;

@end

@implementation PlayerVC
@synthesize musicPlayer;
@synthesize userSelectedPlayList;
- (void)viewDidLoad {
    [super viewDidLoad];
    userDefault=[NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    [userDefault setObject:@"first" forKey:@"selectedSong"];
    
    
    [self setMusicPlayer: [MPMusicPlayerController applicationMusicPlayer]];
    
    // By default, an application music player takes on the shuffle and repeat modes
    //		of the built-in iPod app. Here they are both turned off.
    [musicPlayer setShuffleMode: MPMusicShuffleModeOff];
    [musicPlayer setRepeatMode: MPMusicRepeatModeNone];
    [self registerForMediaPlayerNotifications];
    [self createAndDisplayMPVolumeView];
    alert=[[UIAlertView alloc]initWithTitle:@"Oop's!" message:@"Unable to play purchased song.Create your Playlist again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    //valumeSlider.value=musicPlayer.volume;
   // NSLog(@"volume1=%f",musicPlayer.volume);
}

-(void)viewWillAppear:(BOOL)animated{
    selectedSong=[userDefault valueForKey:@"selectedSong"];
    [self frameSetting];
    
}
-(void)songChanged
{
    int index=[selectedSong intValue];
    NSLog(@"index=%d",index);
    [self sendSongDetail:index];
    [self sendAudio];
    
}



    
    

#pragma mark - Media Picker delegate

- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection
{
    [self dismissViewControllerAnimated:YES completion:nil];
    //self.song = mediaItemCollection.items[0];
    NSLog(@"%@",mediaItemCollection.items);
    
    if(mediaItemCollection)
    {
        //userSelectedPlayList=mediaItemCollection;
        NSLog(@"userSelectedPlayList=%@",userSelectedPlayList);
        [self updatePlayerQueueWithMediaCollection: mediaItemCollection];
        
        
    }
}

- (void) updatePlayerQueueWithMediaCollection: (MPMediaItemCollection *) mediaItemCollection {
    
    // Configure the music player, but only if the user chose at least one song to play
    if (mediaItemCollection) {
        if (userSelectedPlayList == nil) {
            
            NSMutableArray *items = [[NSMutableArray alloc]init];
            
           // MPMediaItem *item;
            for(int i =0;i<mediaItemCollection.count;i++)
            {
                NSString *str=[NSString stringWithFormat:@"%@",[mediaItemCollection.items[i] valueForProperty:MPMediaItemPropertyAssetURL]];
              //  if([str containsString:@"mp3"])
                {
                    NSLog(@"mp3 song=%@",str);
                    //item=[[MPMediaItem alloc]init];
                    [items addObject:mediaItemCollection.items[i]];
                }
                //else
                {
                     NSLog(@"mp4 song=%@",str);
                }
            }
            if(items.count>0)
            {
                [playList setTitle:@"Show Playlist" forState:UIControlStateNormal];
                MPMediaItemCollection *myNewMediaItemCollection = [MPMediaItemCollection collectionWithItems:items];
                [self setUserSelectedPlayList: myNewMediaItemCollection];
                [musicPlayer setQueueWithItemCollection: userSelectedPlayList];
                                
            }else
            {
                [playList setTitle:@"Create Playlist" forState:UIControlStateNormal];
                [alert show];
            }
            //[self setUserSelectedPlayList: mediaItemCollection];
            //[musicPlayer setQueueWithItemCollection: userSelectedPlayList];
            
        }
    }
}

-(void)sendSongDetail:(int)index
{
    self.song = userSelectedPlayList.items[index];
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    info[@"title"] = [self.song valueForProperty:MPMediaItemPropertyTitle] ? [self.song valueForProperty:MPMediaItemPropertyTitle] : @"";
    info[@"artist"] = [self.song valueForProperty:MPMediaItemPropertyArtist] ? [self.song valueForProperty:MPMediaItemPropertyArtist] : @"";
    
    MPMediaItemArtwork *artwork = [self.song valueForProperty:MPMediaItemPropertyArtwork];
    UIImage *image = [artwork imageWithSize:songImage.frame.size];
    UIImage *img=[UIImage imageNamed:@"AlbumMock.png"];
    
    if (image)
        info[@"artwork"] = image;
    
    if (info[@"artwork"])
        songImage.image = info[@"artwork"];
    else
        songImage.image=img;
    
    songName.text = info[@"title"];
    authorName.text = info[@"artist"];
    [self.session sendData:[NSKeyedArchiver archivedDataWithRootObject:[info copy]]];
    
}
-(void)sendAudio
{
    NSArray *peers = [self.session connectedPeers];
    NSString *streamName=[NSString stringWithFormat:@"music%d",count];
    for(int i=0;i<peers.count;i++)
    {
        if(!self.outputStreamer)
        {
            self.outputStreamer = [[TDAudioOutputStreamer alloc] initWithOutputStream:[self.session outputStreamForPeer:peers[i] streamName:streamName]];
        }
        else{
            
            [self.outputStreamer stop];
             self.outputStreamer=nil;
            self.outputStreamer = [[TDAudioOutputStreamer alloc] initWithOutputStream:[self.session outputStreamForPeer:peers[i] streamName:streamName]];
        }
        
        
        [self.outputStreamer streamAudioFromURL:[self.song valueForProperty:MPMediaItemPropertyAssetURL]];
    
        
        
        NSLog(@"MPMediaItemPropertyAssetURL=%@",[self.song valueForProperty:MPMediaItemPropertyAssetURL]);
        //ipod-library://item/item.mp3?id=5434715435250394446
        //ipod-library://item/item.m4a?id=1417463104268679361
        [self.outputStreamer start];
    }
    count++;
    NSLog(@"count=%d",count);
}

- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)homeBtnPressed:(id)sender {
    [self.outputStreamer stop];
     self.outputStreamer=nil;
    self.session=nil;
    [musicPlayer stop];
    musicPlayer=nil;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)playListBtn:(id)sender {
    if(userSelectedPlayList==nil)
    {
        MPMediaPickerController *picker = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeMusic];
        picker.delegate = self;
        picker.allowsPickingMultipleItems=true;
        picker.showsCloudItems=NO;
       
        [self presentViewController:picker animated:YES completion:nil];
        [playList setTitle:@"Show Playlist" forState:UIControlStateNormal];
    }
    else
    {
        
        PlayListViewController *playList1;
        if(!playList1)
            playList1=[[PlayListViewController alloc]init];
        
        playList1.currentQueue=self.userSelectedPlayList;
        //playList.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
        
        [self presentViewController:playList1 animated:YES completion:nil];
        
    }

}



- (IBAction)playBtn:(id)sender {
    if(userSelectedPlayList.count>0)
    {
        int index;
        MPMediaItem *item;
        if([selectedSong isEqualToString:@"first"])
        {
            index=0;
            item =  userSelectedPlayList.items[0];
            currentPlayingSongIndex=0;
            
        }else if([selectedSong isEqualToString:@"no"])
        {
            return;
        }
        else
        {
            index=[selectedSong intValue];
            NSLog(@"index=%d",index);
            item =  userSelectedPlayList.items[index];
            currentPlayingSongIndex=index;
            
        }
        if(musicPlayer.playbackState == MPMusicPlaybackStatePlaying)
        {
            [musicPlayer stop];
            
        }
        [self sendSongDetail:index];
        [self sendAudio];
        [musicPlayer setNowPlayingItem:item];
        [musicPlayer prepareToPlay];
        [musicPlayer play];
        selectedSong=@"no";

    }
    
}
-(void)copyOfplayBtn
{
    if(userSelectedPlayList.count>0)
    {
        MPMediaItem *item;
        // if(isPlaying)
        {
            //[playButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
            //[playButton setTitle:@"Play" forState:UIControlStateNormal];
            // isPlaying=NO;
        }
        //  else
        {
            //   isPlaying=YES;
            // [musicPlayer play];
            //[playButton setTitle:@"Pause" forState:UIControlStateNormal];
            if([selectedSong isEqualToString:@"first"])
            {
                [self sendSongDetail:0];
                item =  userSelectedPlayList.items[0];
                currentPlayingSongIndex=0;
                
            }
            else if([selectedSong isEqualToString:@"no"])
            {
                return;
            }
            else
            {
                int index=[selectedSong intValue];
                NSLog(@"index=%d",index);
                item =  userSelectedPlayList.items[index];
                currentPlayingSongIndex=index;
                [self sendSongDetail:index];
            }
            if(musicPlayer.playbackState == MPMusicPlaybackStatePlaying)
            {
                [musicPlayer stop];
                
            }
            [self sendAudio];
            [musicPlayer setNowPlayingItem:item];
            [musicPlayer prepareToPlay];
            [musicPlayer play];
            
        }
        
    }
    //  selectedSong =@"no";

}
- (void) registerForMediaPlayerNotifications {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver: self
                           selector: @selector (handle_NowPlayingItemChanged:)
                               name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                             object: musicPlayer];
    
    [notificationCenter addObserver: self
                           selector: @selector (handle_PlaybackStateChanged:)
                               name: MPMusicPlayerControllerPlaybackStateDidChangeNotification
                             object: musicPlayer];
    
   
}

#pragma mark Music notification handlers__________________

// When the now-playing item changes, update the media item artwork and the now-playing label.
- (void) handle_NowPlayingItemChanged: (id) notification {
    
}

// When the playback state changes, set the play/pause button in the Navigation bar
//		appropriately.
- (void) handle_PlaybackStateChanged: (id) notification {
    
    MPMusicPlaybackState playbackState = [musicPlayer playbackState];
    
    if (playbackState == MPMusicPlaybackStatePaused) {
         NSLog(@"Song MPMusicPlaybackStatePaused");
       // navigationBar.topItem.leftBarButtonItem = playBarButton;
        
    } else if (playbackState == MPMusicPlaybackStatePlaying) {
         NSLog(@"Song MPMusicPlaybackStatePlaying");
        //navigationBar.topItem.leftBarButtonItem = pauseBarButton;'

        
    } else if (playbackState == MPMusicPlaybackStateStopped) {
         NSLog(@"Song MPMusicPlaybackStateStopped");
        //[self nextSong];
        
    }
}
-(void)nextSong
{
    NSLog(@"Song Changed");
    if(!(isForwardPressed||isBackwardPressed))
    {
        if(currentPlayingSongIndex<userSelectedPlayList.count-1)
        {
            if(musicPlayer.playbackState == MPMusicPlaybackStatePlaying)
            {
                currentPlayingSongIndex++;
                [userDefault setObject:@"no" forKey:@"selectedSong"];
                [self sendSongDetail:(int)currentPlayingSongIndex];
                [self sendAudio];
                
            }
        }
        isForwardPressed=0;
        isBackwardPressed=0;
        
    }

}


- (IBAction)backward:(id)sender {
    
    if(currentPlayingSongIndex>0)
    {
        if(musicPlayer.playbackState == MPMusicPlaybackStatePlaying)
        {
            isBackwardPressed=1;
            currentPlayingSongIndex--;
            [self currentSong];
            
        }
        
    }

}

- (IBAction)forward:(id)sender {
    
    if(currentPlayingSongIndex<userSelectedPlayList.count-1)
    {
        if(musicPlayer.playbackState == MPMusicPlaybackStatePlaying)
        {
            isForwardPressed=1;
             currentPlayingSongIndex++;
             [self currentSong];
        }

       
    }

}

-(void)currentSong
{
        [musicPlayer stop];
        [userDefault setObject:@"no" forKey:@"selectedSong"];
         selectedSong=@"no";
    
        [self sendSongDetail:(int)currentPlayingSongIndex];
        [self sendAudio];
        
        MPMediaItem *item=userSelectedPlayList.items[currentPlayingSongIndex];
        [musicPlayer setNowPlayingItem:item];
        [musicPlayer prepareToPlay];
        [musicPlayer play];
    
}

-(void)frameSetting
{
    [backgroundImage setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    int height=SCREEN_HEIGHT;
    
    switch(height)
    {
            
        case 480:
            [songName setFrame:CGRectMake(20,235, self.view.frame.size.width-40,songName.frame.size.height)];
            [authorName setFrame:CGRectMake(20,260, self.view.frame.size.width-40,authorName.frame.size.height)];
            [songImage setFrame:CGRectMake(20,70, self.view.frame.size.width-40,150)];
            [playBtn setFrame:CGRectMake(150,290, playBtn.frame.size.width,playBtn.frame.size.height)];
            [backwardBtn setFrame:CGRectMake(60,293, backwardBtn.frame.size.width,backwardBtn.frame.size.height)];
            [forward setFrame:CGRectMake(220,293, forward.frame.size.width,forward.frame.size.height)];
            [homeBtn setFrame:CGRectMake(130, self.view.frame.size.height-homeBtn.frame.size.height-60, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            [volumeHolder setFrame:CGRectMake(40,325, volumeHolder.frame.size.width,volumeHolder.frame.size.height)];
            [minVolumeImage setFrame:CGRectMake(20,330, minVolumeImage.frame.size.width,minVolumeImage.frame.size.height)];
            [maxValumeImage setFrame:CGRectMake(270,328, maxValumeImage.frame.size.width,maxValumeImage.frame.size.height)];
            [playList setFrame:CGRectMake(self.view.frame.size.width-playList.frame.size.width-20, 25, playList.frame.size.width,playList.frame.size.height)];
            break;
            
        case 736:
            [playList setFrame:CGRectMake(self.view.frame.size.width-playList.frame.size.width-20, 25, playList.frame.size.width,playList.frame.size.height)];
            [playBtn setFrame:CGRectMake(180,420, playBtn.frame.size.width,playBtn.frame.size.height)];
            [backwardBtn setFrame:CGRectMake(80,423, backwardBtn.frame.size.width,backwardBtn.frame.size.height)];
            [forward setFrame:CGRectMake(240,423, forward.frame.size.width,forward.frame.size.height)];
            [songName setFrame:CGRectMake(20,380, self.view.frame.size.width-40,songName.frame.size.height)];
            [authorName setFrame:CGRectMake(20,410, self.view.frame.size.width-40,authorName.frame.size.height)];
            [songImage setFrame:CGRectMake(self.view.frame.origin.x+20, songImage.frame.origin.y, self.view.frame.size.width-40,280)];
            [homeBtn setFrame:CGRectMake(175, self.view.frame.size.height-homeBtn.frame.size.height-90, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            [volumeHolder setFrame:CGRectMake(40,460, volumeHolder.frame.size.width,volumeHolder.frame.size.height)];
            [minVolumeImage setFrame:CGRectMake(20,465, minVolumeImage.frame.size.width,minVolumeImage.frame.size.height)];
            [maxValumeImage setFrame:CGRectMake(270,463, maxValumeImage.frame.size.width,maxValumeImage.frame.size.height)];
            break;
            
        case 667:
            [playList setFrame:CGRectMake(self.view.frame.size.width-playList.frame.size.width-20, 25, playList.frame.size.width,playList.frame.size.height)];
            [playBtn setFrame:CGRectMake(180,420, playBtn.frame.size.width,playBtn.frame.size.height)];
            [backwardBtn setFrame:CGRectMake(80,423, backwardBtn.frame.size.width,backwardBtn.frame.size.height)];
            [forward setFrame:CGRectMake(240,423, forward.frame.size.width,forward.frame.size.height)];
            [songName setFrame:CGRectMake(20,380, self.view.frame.size.width-40,songName.frame.size.height)];
            [authorName setFrame:CGRectMake(20,410, self.view.frame.size.width-40,authorName.frame.size.height)];
            [songImage setFrame:CGRectMake(self.view.frame.origin.x+20, songImage.frame.origin.y, self.view.frame.size.width-40,280)];
            [homeBtn setFrame:CGRectMake(160, self.view.frame.size.height-homeBtn.frame.size.height-80, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            [volumeHolder setFrame:CGRectMake(40,460, volumeHolder.frame.size.width,volumeHolder.frame.size.height)];
            [minVolumeImage setFrame:CGRectMake(20,465, minVolumeImage.frame.size.width,minVolumeImage.frame.size.height)];
            [maxValumeImage setFrame:CGRectMake(270,463, maxValumeImage.frame.size.width,maxValumeImage.frame.size.height)];
            break;
            
    }
    
}

- (void)dealloc {
    
       [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                                                  object: musicPlayer];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: MPMusicPlayerControllerPlaybackStateDidChangeNotification
                                                  object: musicPlayer];
    
   /* [[NSNotificationCenter defaultCenter] removeObserver: self
                                                    name: @"AVSystemController_SystemVolumeDidChangeNotification"
                                                  object: nil];*/
    
    
    [musicPlayer endGeneratingPlaybackNotifications];
    musicPlayer=nil;
}

/*- (void)volumeChanged:(NSNotification *)notification
{
    float volume =
    [[[notification userInfo]
      objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
     floatValue];
    NSLog(@"volume=%f",volume);
    valumeSlider.value=volume;
}*/

-(void) createAndDisplayMPVolumeView
{
    // Create a simple holding UIView and give it a frame
    volumeHolder = [[UIView alloc] initWithFrame: CGRectMake(40, 385, 220,30)];
    [volumeHolder setBackgroundColor: [UIColor clearColor]];
    
    
    [self.view addSubview: volumeHolder];
    
    // Create an instance of MPVolumeView and give it a frame
    MPVolumeView *myVolumeView = [[MPVolumeView alloc] initWithFrame: volumeHolder.bounds];
    
    // Add myVolumeView as a subView of the volumeHolder
    [volumeHolder addSubview: myVolumeView];
    myVolumeView.showsVolumeSlider=true;
    
}

@end
