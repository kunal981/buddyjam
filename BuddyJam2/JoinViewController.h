//
//  JoinViewController.h
//  BuddyJam2
//
//  Created by brst on 1/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PeerSession.h"
#import "TDAudioStreamer.h"
@import MediaPlayer;
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface JoinViewController : UIViewController<PeerSessionDelegate>
{
    
    IBOutlet UIImageView *maxVolumeImage;
    IBOutlet UIImageView *backgroundImage;
    IBOutlet UIButton *homeBtn;
    IBOutlet UILabel *groupName;
    IBOutlet UILabel *authorName;
    IBOutlet UILabel *songName;
    IBOutlet UIImageView *audioImg;
    UIView *volumeHolder;

    IBOutlet UIImageView *minVolumeImage;
}
@property (strong, nonatomic) PeerSession *session;
@property (strong, nonatomic) TDAudioInputStreamer *inputStream;

- (IBAction)homeBtnPressed:(id)sender;

@end
