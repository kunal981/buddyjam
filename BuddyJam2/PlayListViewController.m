//
//  PlayListViewController.m
//  BuddyJam
//
//  Created by brst on 1/12/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "PlayListViewController.h"
#import "PlayerVC.h"



@interface PlayListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    PlayerVC *selectedVC;
    NSUserDefaults *userDefault;
    
}

@end

@implementation PlayListViewController
@synthesize currentQueue;
- (void)viewDidLoad {
    [super viewDidLoad];
    selectedVC=[[PlayerVC alloc]init];
    self.playList.delegate=self;
    self.playList.dataSource=self;
    
    userDefault=[NSUserDefaults standardUserDefaults];
    [userDefault synchronize];
    
    NSLog(@"currentQueue=%@",currentQueue);
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self frameSetting];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [currentQueue.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        
    }
    
    MPMediaItem *anItem = (MPMediaItem *)[currentQueue.items objectAtIndex:indexPath.row];
    if (anItem) {
        cell.textLabel.text = [anItem valueForProperty:MPMediaItemPropertyTitle];
    }
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *count=[NSString stringWithFormat:@"%d",(int)indexPath.row];
    [userDefault setObject:count forKey:@"selectedSong"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)DonePressed:(id)sender {
   
    [userDefault setObject:@"no" forKey:@"selectedSong"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)frameSetting
{
    //int height=SCREEN_HEIGHT;
    [self.playList setFrame:CGRectMake(20, 70, self.view.frame.size.width-40,self.view.frame.size.height-120)];
    [doneBtn setFrame:CGRectMake(self.view.frame.size.width-doneBtn.frame.size.width-20,30, doneBtn.frame.size.width,doneBtn.frame.size.height)];
    [musicLbl setFrame:CGRectMake(0,30, self.view.frame.size.width,musicLbl.frame.size.height)];
    
}

@end
