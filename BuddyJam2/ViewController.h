//
//  ViewController.h
//  BuddyJam2
//
//  Created by brst on 1/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
@class CreateViewController;
@interface ViewController : UIViewController
{
    CreateViewController *createObj;
    IBOutlet UIButton *joinBtn;
    IBOutlet UIButton *createBtn;
    IBOutlet UIImageView *backgroundImage;
}
- (IBAction)createBtnPressed:(id)sender;
- (IBAction)joinBtnPressed:(id)sender;


@end

