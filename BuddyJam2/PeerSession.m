//
//  PeerSession.m
//  BuddyJam
//
//  Created by brst on 1/2/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "PeerSession.h"

@import MultipeerConnectivity;

@implementation PeerSession
- (instancetype)initWithPeerDisplayName:(NSString *)name
{
    self = [super init];
    if (!self) return nil;
    
    self.peerID = [[MCPeerID alloc] initWithDisplayName:name];
    return self;
}


#pragma mark - Properties


- (MCSession *)session
{
    streamName1=@"1";
     //alert=[[UIAlertView alloc]initWithTitle:@"Opp's!" message:@"123" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Reconnect", nil];
     alert=[[UIAlertView alloc]initWithTitle:@"Opp's!" message:@"123" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];


    
    if (!_session) {
        _session = [[MCSession alloc] initWithPeer:self.peerID];
        _session.delegate = self;
    }
    return _session;
}

#pragma mark - MCSessionDelegate methods

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    
    if (state == MCSessionStateConnecting) {
        NSLog(@"Connecting to %@", peerID.displayName);
        
     
    } else if (state == MCSessionStateConnected) {
        NSLog(@"Connected to %@", peerID.displayName);
        
        
    } else if (state == MCSessionStateNotConnected) {
        _dicsconnectedPeerID=peerID;
        NSLog(@"Disconnected from %@", peerID.displayName);
//        alert.message=[NSString stringWithFormat:@"%@ Disconnected",peerID.displayName];
//        if([showAlert isEqualToString:@"1"])
//          [alert show];
    }
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
    showAlert=@"1";
    [self.delegate session:self didReceiveData:data];
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress
{
    
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error
{
    
}


- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID
{
   // if ([streamName isEqualToString:streamName1])
    if(streamName)
    {
        [self.delegate session:self didReceiveAudioStream:stream];
    }

}

- (NSOutputStream *)outputStreamForPeer:(MCPeerID *)peer streamName:(NSString *)streamName
{
    streamName1=[NSString stringWithFormat:@"%@",streamName];
    NSError *error;
   // NSOutputStream *stream = [self.session startStreamWithName:@"music" toPeer:peer error:&error];
    NSOutputStream *stream = [self.session startStreamWithName:streamName toPeer:peer error:&error];
    if (error) {
        NSLog(@"Error: %@", [error userInfo].description);
    }
    
    return stream;
}

#pragma mark - Advertising Assistant

- (void)startAdvertisingForServiceType:(NSString *)type discoveryInfo:(NSDictionary *)info
{
    if (!self.advertiser)
        self.advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:type discoveryInfo:info session:self.session];
    
    [self.advertiser start];
}

- (void)stopAdvertising
{
    [self.advertiser stop];
}

- (NSArray *)connectedPeers
{
    NSLog(@"%@",[self.session connectedPeers]);
    return [self.session connectedPeers];
}

#pragma mark - MCBrowserViewController delegate

- (void)browserViewControllerDidFinish:(MCBrowserViewController *)browserViewController
{
    showAlert=@"1";
    [browserViewController dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"%@",[self.session connectedPeers]);
}

- (void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController
{
    showAlert=@"1";
    [browserViewController dismissViewControllerAnimated:YES completion:nil];
}

- (MCBrowserViewController *)browserViewControllerForSeriviceType:(NSString *)type
{
    showAlert=@"0";
    MCBrowserViewController *view = [[MCBrowserViewController alloc] initWithServiceType:type session:self.session];
    view.delegate = self;
    return view;
}
- (void)sendData:(NSData *)data
{
    NSError *error;
    [self.session sendData:data toPeers:self.session.connectedPeers withMode:MCSessionSendDataReliable error:&error];
    if (error) {
        NSLog(@"Error: %@", error.userInfo.description);
    }

}
/*- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if(buttonIndex==1)
    {
        NSLog(@"_dicsconnectedPeerID.displayName=%@",_dicsconnectedPeerID.displayName);
        [self.session cancelConnectPeer:_dicsconnectedPeerID];
        NSLog(@"%@",[self.session connectedPeers]);
        self.peerID = [[MCPeerID alloc] initWithDisplayName:[NSString stringWithFormat:@"%@",_dicsconnectedPeerID.displayName]];
        
    }
}*/

@end
