//
//  JoinViewController.m
//  BuddyJam2
//
//  Created by brst on 1/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "JoinViewController.h"
#import "ViewController.h"


@interface JoinViewController ()

@end

@implementation JoinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.session = [[PeerSession alloc] initWithPeerDisplayName :[UIDevice currentDevice].name];
    [self.session startAdvertisingForServiceType:@"dance-party" discoveryInfo:nil];
    self.session.delegate = self;
    
  
    [self createAndDisplayMPVolumeView];

    
}

- (void)changeSongInfo:(NSDictionary *)info
{
    UIImage *img=[UIImage imageNamed:@"AlbumMock.png"];
    if (info[@"artwork"])
        audioImg.image = info[@"artwork"];
    else
        audioImg.image=img;
    
    songName.text = info[@"title"];
    authorName.text = info[@"artist"];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self frameSetting];
    
}


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.session stopAdvertising];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)homeBtnPressed:(id)sender {
    [self.inputStream stop];
    self.inputStream=nil;
    self.session=nil;
    [self.navigationController popToRootViewControllerAnimated:YES];
    //[self.navigationController popViewControllerAnimated:YES];

}

#pragma mark - TDSessionDelegate

- (void)session:(PeerSession *)session didReceiveData:(NSData *)data
{
    NSDictionary *info = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    [self performSelectorOnMainThread:@selector(changeSongInfo:) withObject:info waitUntilDone:NO];
}

- (void)session:(PeerSession *)session didReceiveAudioStream:(NSInputStream *)stream
{
    if (!self.inputStream) {
        self.inputStream = [[TDAudioInputStreamer alloc] initWithInputStream:stream];
    }
    else
    {
        [self.inputStream stop];
       // self.inputStream=nil;
        self.inputStream = [[TDAudioInputStreamer alloc] initWithInputStream:stream];
    }
    [self.inputStream start];
    
}

-(void)frameSetting
{
    [backgroundImage setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    int height=SCREEN_HEIGHT;
    
    switch(height)
    {
            
        case 480:
           
            [songName setFrame:CGRectMake(20,270, self.view.frame.size.width-40,songName.frame.size.height)];
            [authorName setFrame:CGRectMake(20,290, self.view.frame.size.width-40,authorName.frame.size.height)];
            [audioImg setFrame:CGRectMake(20,80, self.view.frame.size.width-40,180)];
            [groupName setFrame:CGRectMake(self.view.frame.size.width-groupName.frame.size.width-10, groupName.frame.origin.y, groupName.frame.size.width,groupName.frame.size.height)];
            [homeBtn setFrame:CGRectMake(130, self.view.frame.size.height-homeBtn.frame.size.height-60, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            [volumeHolder setFrame:CGRectMake(40,320, volumeHolder.frame.size.width,volumeHolder.frame.size.height)];
            [minVolumeImage setFrame:CGRectMake(20,325, minVolumeImage.frame.size.width,minVolumeImage.frame.size.height)];
            [maxVolumeImage setFrame:CGRectMake(270,323, maxVolumeImage.frame.size.width,maxVolumeImage.frame.size.height)];
            break;
      
        case 736:
            
            [songName setFrame:CGRectMake(20,420, self.view.frame.size.width-40,songName.frame.size.height)];
            [authorName setFrame:CGRectMake(20,450, self.view.frame.size.width-40,authorName.frame.size.height)];
            [audioImg setFrame:CGRectMake(self.view.frame.origin.x+20, audioImg.frame.origin.y, self.view.frame.size.width-40,280)];
            [groupName setFrame:CGRectMake(self.view.frame.size.width-groupName.frame.size.width-10, groupName.frame.origin.y, groupName.frame.size.width,groupName.frame.size.height)];
            [homeBtn setFrame:CGRectMake(175, self.view.frame.size.height-homeBtn.frame.size.height-90, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            [volumeHolder setFrame:CGRectMake(40,490, volumeHolder.frame.size.width,volumeHolder.frame.size.height)];
            [minVolumeImage setFrame:CGRectMake(20,495, minVolumeImage.frame.size.width,minVolumeImage.frame.size.height)];
            [maxVolumeImage setFrame:CGRectMake(270,493, maxVolumeImage.frame.size.width,maxVolumeImage.frame.size.height)];
            break;

        
        case 667:
            [songName setFrame:CGRectMake(20, 390, self.view.frame.size.width-40,songName.frame.size.height)];
            [authorName setFrame:CGRectMake(20, 420, self.view.frame.size.width-40,authorName.frame.size.height)];
            [groupName setFrame:CGRectMake(self.view.frame.size.width-groupName.frame.size.width-10, groupName.frame.origin.y, groupName.frame.size.width,groupName.frame.size.height)];
            [audioImg setFrame:CGRectMake(self.view.frame.origin.x+20, audioImg.frame.origin.y, self.view.frame.size.width-40,280)];
            [homeBtn setFrame:CGRectMake(160, self.view.frame.size.height-homeBtn.frame.size.height-80, homeBtn.frame.size.width,homeBtn.frame.size.height)];
            [volumeHolder setFrame:CGRectMake(40,490, volumeHolder.frame.size.width,volumeHolder.frame.size.height)];
            [minVolumeImage setFrame:CGRectMake(20,495, minVolumeImage.frame.size.width,minVolumeImage.frame.size.height)];
            [maxVolumeImage setFrame:CGRectMake(270,493, maxVolumeImage.frame.size.width,maxVolumeImage.frame.size.height)];
            break;
            
    }
    
}

-(void) createAndDisplayMPVolumeView
{
    // Create a simple holding UIView and give it a frame
    volumeHolder = [[UIView alloc] initWithFrame: CGRectMake(40, 385, 220, 30)];
    [volumeHolder setBackgroundColor: [UIColor clearColor]];
    
  
    [self.view addSubview: volumeHolder];
    
    // Create an instance of MPVolumeView and give it a frame
    MPVolumeView *myVolumeView = [[MPVolumeView alloc] initWithFrame: volumeHolder.bounds];
    
    // Add myVolumeView as a subView of the volumeHolder
    [volumeHolder addSubview: myVolumeView];
    myVolumeView.showsVolumeSlider=YES;
}

@end
