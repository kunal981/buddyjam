//
//  ViewController.m
//  BuddyJam2
//
//  Created by brst on 1/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ViewController.h"
#import "CreateViewController.h"
#import "JoinViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    createBtn.layer.cornerRadius=createBtn.frame.size.width/2;
    joinBtn.layer.cornerRadius=joinBtn.frame.size.width/2;
    self.navigationController.navigationBarHidden=YES;
    

   

}
-(void)viewWillAppear:(BOOL)animated{
    
     [self frameSetting];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
   
}

- (IBAction)createBtnPressed:(id)sender {
    
   /*/ if ([UIScreen mainScreen].bounds.size.height==568) {
         createObj=[[CreateViewController alloc]initWithNibName:@"CreateViewController~6+" bundle:nil];
    }else{
     createObj=[[CreateViewController alloc]initWithNibName:@"CreateViewController" bundle:nil];
    }*/
     createObj=[[CreateViewController alloc]initWithNibName:@"CreateViewController" bundle:nil];
 
    [self.navigationController pushViewController:createObj animated:YES];
}

- (IBAction)joinBtnPressed:(id)sender {
    JoinViewController *joinObj=[[JoinViewController alloc]init];
    [self.navigationController pushViewController:joinObj animated:YES];
}
-(void)frameSetting
{
    [backgroundImage setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    int height=SCREEN_HEIGHT;
  
    switch(height)
    {
            
       case 480:
             [createBtn setFrame:CGRectMake(createBtn.frame.origin.x, 220, createBtn.frame.size.height, createBtn.frame.size.width)];
             [joinBtn setFrame:CGRectMake(joinBtn.frame.origin.x,220, joinBtn.frame.size.height, joinBtn.frame.size.width)];
            break;
            
        case 736:
            [createBtn setFrame:CGRectMake(self.view.frame.origin.x+50, 350, createBtn.frame.size.height, createBtn.frame.size.width)];
            [joinBtn setFrame:CGRectMake(self.view.frame.size.width-joinBtn.frame.size.width-50, 350, joinBtn.frame.size.height, joinBtn.frame.size.width)];
            break;
            
        case 667:
            [createBtn setFrame:CGRectMake(self.view.frame.origin.x+50, 325, createBtn.frame.size.height, createBtn.frame.size.width)];
            [joinBtn setFrame:CGRectMake(self.view.frame.size.width-joinBtn.frame.size.width-50, 325, joinBtn.frame.size.height, joinBtn.frame.size.width)];
            break;
            
           
        case 1024:
            [createBtn setFrame:CGRectMake(200, self.view.frame.size.height/2, 120, 120)];
            [joinBtn setFrame:CGRectMake(450, self.view.frame.size.height/2,120, 120)];
            createBtn.layer.cornerRadius=createBtn.frame.size.width/2;
            joinBtn.layer.cornerRadius=joinBtn.frame.size.width/2;

    }
    
}
@end
