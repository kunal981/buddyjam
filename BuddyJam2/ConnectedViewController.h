//
//  ConnectedViewController.h
//  BuddyJam2
//
//  Created by brst on 1/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PeerSession.h"
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface ConnectedViewController : UIViewController
{
    
    IBOutlet UIImageView *buddyImage;
    IBOutlet UIImageView *backgroundImage;
    IBOutlet UITableView *tableView;
    
    IBOutlet UIButton *homeBtn;
    IBOutlet UILabel *connectedLbl;
    IBOutlet UIButton *nextButton;
}
@property (strong, nonatomic) PeerSession *session;
- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)nextBtnPressed:(id)sender;

@end
