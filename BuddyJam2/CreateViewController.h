//
//  CreateViewController.h
//  BuddyJam2
//
//  Created by brst on 1/15/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@interface CreateViewController : UIViewController
{
    
    IBOutlet UILabel *passwordLbl;
    IBOutlet UILabel *createLbl;
    IBOutlet UIImageView *buddyImage;
    IBOutlet UIImageView *backgroundImage;
    IBOutlet UITextField *groupPassword;
    IBOutlet UITextField *groupName;
    IBOutlet UIButton *startBtn;
    IBOutlet UISwitch *switchBtn;
    IBOutlet UIButton *homeBtn;
    
}
- (IBAction)homeBtnPressed:(id)sender;

- (IBAction)startBtnPressed:(id)sender;
@end
    