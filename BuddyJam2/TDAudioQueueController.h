//
//  TDAudioQueueController.h
//  TDAudioStreamer
//


#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface TDAudioQueueController : NSObject

+ (OSStatus)playAudioQueue:(AudioQueueRef)audioQueue;
+ (OSStatus)pauseAudioQueue:(AudioQueueRef)audioQueue;
+ (OSStatus)stopAudioQueue:(AudioQueueRef)audioQueue;
+ (OSStatus)finishAudioQueue:(AudioQueueRef)audioQueue;

@end
