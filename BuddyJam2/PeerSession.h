//
//  PeerSession.h
//  BuddyJam
//
//  Created by brst on 1/2/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MultipeerConnectivity;
@class PeerSession, MCPeerID, MCBrowserViewController;
@protocol PeerSessionDelegate <NSObject>

- (void)session:(PeerSession *)session didReceiveData:(NSData *)data;
- (void)session:(PeerSession *)session didReceiveAudioStream:(NSInputStream *)stream;

@end


@interface PeerSession : NSObject<MCSessionDelegate, MCBrowserViewControllerDelegate,UIAlertViewDelegate>
{
    UIAlertView *alert;
    NSString *showAlert;
    NSString *streamName1;
}
@property (weak, nonatomic) id<PeerSessionDelegate> delegate;
@property (strong, nonatomic) MCSession *session;
@property (strong, nonatomic) MCAdvertiserAssistant *advertiser;
@property (strong, nonatomic) MCPeerID *peerID,*dicsconnectedPeerID;
- (void)startAdvertisingForServiceType:(NSString *)type discoveryInfo:(NSDictionary *)info;
- (void)stopAdvertising;
- (MCBrowserViewController *)browserViewControllerForSeriviceType:(NSString *)type;
- (instancetype)initWithPeerDisplayName:(NSString *)name;
- (NSArray *)connectedPeers;
- (NSOutputStream *)outputStreamForPeer:(MCPeerID *)peer streamName:(NSString *)streamName;

- (void)sendData:(NSData *)data;
@end
